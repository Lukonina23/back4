<?php

header('Content-Type: text/html; charset=UTF-8');

$abilities = array(
  'immortality' => "Бессмертие",
  'levitation' => "Левитация",
  'invisibility"' => "Невидимость");

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['pol'] = !empty($_COOKIE['pol_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    if ($_COOKIE['fio_error'] == "1") {
      $messages[] = '<div class="error">Заполните имя.</div>';
    }
    else {
      $messages[] = '<div class="error">Укажите корректное имя.</div>';
    }
    
  }
    if ($errors['email']) {
      setcookie('email_error', '', 100000);
      if ($_COOKIE['email_error'] == "1") {
        $messages[] = '<div class="error">Заполните email.</div>';
      }
      else {
        $messages[] = '<div class="error">Укажите корректный email.</div>';
    }

    if ($errors['year']) {
      setcookie('year_error', '', 100000);
      if ($_COOKIE['year_error'] == "1") {
        $messages[] = '<div class="error">Заполните год.</div>';
      }
      else {
        $messages[] = '<div class="error">Укажите корректный год.</div>';
      }
    }
    
    if ($errors['checkbox']) {
      setcookie('checkbox_error', '', 100000);
      if ($_COOKIE['checkbox_error'] == "1") {
        $messages[] = '<div class="error">Вы не приняли соглашение.</div>';
      }  
    }
     if ($errors['biography']) {
      setcookie('biography_error', '', 100000);
      $messages[] = '<div class="error">Заполните текстовое поле.</div>';
    }


    if ($errors['abilities']) {

      setcookie('abilities_error', '', 100000);

      if ($_COOKIE['abilities_error'] == "1") {
        $messages[] = '<div class="error">Выберите споособность</div>';
      }
      else {
        $messages[] = '<div class="error">Выбрана недопустимая способность</div>';
      }
      
    }

  }

  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) || !preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['pol'] = $_COOKIE['pol_value'] === '0' ? 0 : 1;
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  if (!empty($_COOKIE['abilities_value'])) {
    $abilities_value = json_decode($_COOKIE['abilities_value']);
  }
  $values['abilities'] = array();
  if (is_array($abilities_value)) {
    foreach($abilities_value as $ability) {
      if (!empty($abilities[$ability])) {
        $values['abilities'][$ability] = $ability;
      }
    }
  }
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : $_COOKIE['checkbox_value'];
  include('form.php');
}
else {
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if (!preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_POST['fio'])) {
      setcookie('fio_error', '2', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if (!preg_match('/^[^@]+@[^@.]+\.[^@]+$/', $_POST['email'])) {
      setcookie('email_error', '2', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['year'])) {
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $year = $_POST['year'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) {
      setcookie('year_error', '2', time() + 24 * 60 * 60);  
      $errors = TRUE;
    }
    setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }

  setcookie('pol_value', $_POST['pol'], time() + 12 * 30 * 24 * 60 * 60);

  if (empty($_POST['limbs'])) {
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
  }
  if (empty($_POST['abilities'])) {
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $abilities_error = FALSE;
    foreach($_POST['abilities'] as $key) {
      if (empty($abilities[$key])) {
        setcookie('abilities_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
        $abilities_error = TRUE;
      }
    }
    if (!$abilities_error) {
      setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
    }
  }

  if (empty($_POST['biography'])) {
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('biography_value', $_POST['biography'], time() + 12 * 30 * 24 * 60 * 60);
  }
  if (!isset($_POST['checkbox'])) {
    setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('checkbox_value', $_POST['checkbox'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('pol_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('checkbox_error', '', 100000);
  }

  $user = 'u20948';
  $pass = '6000592';
  $db = new PDO('mysql:host=localhost;dbname=u20948', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
  try {
  $str = implode(',',$_POST['abilities']);
  $stmt = $db->prepare("INSERT INTO appl SET fio = ?, email = ?, yob = ?, pol = ?, limb = ?, biograghy = ?");
  $stmt -> execute(array($_POST['fio'], $_POST['email'], intval($_POST['year']), intval($_POST['pol']), intval($_POST['limbs']), $_POST['biography']));
  $stmt = $db->prepare("INSERT INTO abilities SET abilities = ?");
  $stmt -> execute([$str]);
  }
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}
  setcookie('save', '1');
  header('Location: index.php');
} 

